
{extends file='checkout/checkout.tpl'}

{block name='content'}

    <form id="payl8rForm" target="payl8rFrame" action="{$action}" method="post">
      <input type="hidden" name="data" value="{$data}"/>
      <input type="hidden" name="rid" value="{$rid}">  
      <input type="submit" style="display:none">
    </form>
    <iframe width="100%" height="100%" src="" name="payl8rFrame" style="min-height:600px; border: none;margin:0"></iframe>
    
{/block}

{block name='javascript_bottom'}
  {$smarty.block.parent}
    <script type="text/javascript">
      (function () {
        window.addEventListener("message", pl_iframe_heightUpdate, false);
        var prevHeight = $('[name="payl8rFrame"]').height();
        function pl_iframe_heightUpdate(event) {
          var origin = event.origin || event.originalEvent.origin;
          if (origin !== "https://payl8r.com")
            return;
          if (prevHeight !== $('[name="payl8rFrame"]').height())
            prevHeight = event.data;
            $('[name="payl8rFrame"]').height(event.data+80);
        }
        document.getElementById("payl8rForm").submit();
      })();
    </script>
{/block}

