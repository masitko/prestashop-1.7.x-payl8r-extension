<?php

class Payl8rValidationModuleFrontController extends ModuleFrontController
{	
	public function postProcess()
	{
		$status = Tools::getValue('status');
		$cart = $this->context->cart;
		if ($cart->id_customer == 0 || $cart->id_address_delivery == 0 || $cart->id_address_invoice == 0 || !$this->module->active)
			Tools::redirect('index.php?controller=order&step=1');

    $cart_id = Tools::getValue('cart_id');
    $id_order = Order::getOrderByCartId((int)$cart_id);
    $order = new Order($id_order);

		$authorized = false;
		foreach (Module::getPaymentModules() as $module)
			if ($module['name'] == 'payl8r')
			{
				$authorized = true;
				break;
			}
		if (!$authorized)
			die($this->module->l('This payment method is not available.', 'validation'));

		$customer = new Customer($cart->id_customer);
		if (!Validate::isLoadedObject($customer))
			Tools::redirect('index.php?controller=order&step=1');

		if( $status === 'success' ) {
			Tools::redirect('index.php?controller=order-confirmation&id_cart='.(int)$cart_id.'&id_module='.(int)$this->module->id.'&id_order='.$id_order.'&key='.$customer->secure_key);
		}
		else {
      // retrieve the old cart in case of failed payment 
      $oldCart = new Cart($cart_id);
      $newCart = $oldCart->duplicate();
      if( $newCart['success'] ) {
        $new_id = $newCart['cart']->id;
        $this->context->cart = new Cart($new_id);
        $this->context->cookie->id_cart = $new_id;
      }
			return $this->setTemplate('module:payl8r/views/templates/front/order-fail.tpl');			
		}
	}
}
