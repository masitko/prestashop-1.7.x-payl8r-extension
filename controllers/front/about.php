<?php

class Payl8rAboutModuleFrontController extends ModuleFrontController
{

  public $display_column_left = false;
  public $display_column_right = false;

  public function initContent()
  {
    parent::initContent();

    $this->context->smarty->assign(array(
      'this_path_payl8r' => _MODULE_DIR_ . 'payl8r/',
    ));

    $this->setTemplate('module:payl8r/views/templates/front/about.tpl');
  }

  public function setMedia()
  {
    parent::setMedia();

    $this->registerStylesheet(
      'module-payl8r-style',
      'modules/' . $this->module->name . '/views/css/style.css',
      [
        'media' => 'all',
        'priority' => 200,
      ]
    );

    $this->registerStylesheet(
      'module-payl8r-calc-style',
      'modules/' . $this->module->name . '/views/css/pl-calculator.css',
      [
        'media' => 'all',
        'priority' => 200,
      ]
    );

    $this->registerJavascript(
      'module-payl8r-calc-js',
      'modules/' . $this->module->name . '/views/js/pl-calculator.js',
      [
        'priority' => 200,
        'attribute' => 'async',
      ]
    );
  }



}
